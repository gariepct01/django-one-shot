# Generated by Django 4.2 on 2023-04-18 21:48

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0005_rename_list_instance_todoitem_list"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="list",
            new_name="list_instance",
        ),
    ]
