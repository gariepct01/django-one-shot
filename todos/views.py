from django.shortcuts import render, get_object_or_404, redirect
from todos.forms import TodoListForm, TodoItemForm
from todos.models import TodoList, TodoItem
from django.db import models


# Create your views here.
def todo_list_list(request):
    list_list = TodoList.objects.all()
    context = {"list_list": list_list}
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    list_instance = get_object_or_404(TodoList, id=id)
    context = {"list_instance": list_instance}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list_instance = form.save(False)
            list_instance.created_on = models.DateTimeField(auto_now_add=True)
            list_instance = form.save()
            return redirect("todo_list_detail", id=list_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list_instance)
        if form.is_valid():
            list_instance = form.save()
            return redirect("todo_list_detail", id=list_instance.id)
    else:
        form = TodoListForm(instance=list_instance)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_item_update(request, id):
    item_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_instance)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item_instance)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
